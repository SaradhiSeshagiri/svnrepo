//Interface for various login functionalities..
public interface LoginFunctionalities {

	//Abstract methods...
	public void loginIntoWebExSession(int sessionId);
	//public void loginIntoUdemy(String userName, String password);

	public void logOut();

}//End of the interface